import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';

import useCachedResources from './hooks/useCachedResources';


import { Root } from "native-base";
import { Router, Scene, Stack} from 'react-native-router-flux';

import Initial from './screens/Initial';
import Home from './screens/Home';
import Login from './screens/Login';
import PhoneLogin from './screens/PhoneLogin';
import ForgotPwd from './screens/ForgotPwd';
import ResetPwd from './screens/ResetPwd';
import Signup from './screens/Signup';
export default function App(props) {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
        <Root>
          <Router>    
            <Stack>
              <Scene key="initial" component={Initial} />
              <Scene key="home" component={Home} />
              <Scene key="login" component={Login} initial/>
              <Scene key="phonelogin" component={PhoneLogin} />
              <Scene key="forgotpwd" component={ForgotPwd} />
              <Scene key="resetpwd" component={ResetPwd} />
              <Scene key="signup" component={Signup} />
            </Stack>
          </Router>
        </Root>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
