import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';
import { Text } from 'react-native';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

export default function BottomTabNavigator({ navigation, route }) {
  
  navigation.setOptions({ headerTitle: getHeaderTitle(route), headerShown: false});

  return (
    <BottomTab.Navigator >
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'ホーム',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="home" type="feather" />,
        }}
      />
      <BottomTab.Screen
        name="Order"
        component={LinksScreen}
        options={{
          title: '私の注文',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="shopping-bag" type="feather" />,
        }}
      />
      <BottomTab.Screen
        name="Message"
        component={LinksScreen}
        options={{
          title: 'メッセージ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="mail" type="feather" />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={LinksScreen}
        options={{
          title: '私の情報',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="account-circle-outline" type="material" />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'Home':
      return 'ホーム';
    case 'Order':
      return '私の注文';
    case 'Message':
      return 'メッセージ';
    case 'Profile':
      return '私の情報';
  }
}
