import React from 'react';
import { StyleSheet, View, TouchableOpacity, Platform, Text, FlatList} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Constants from "expo-constants";
export default class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            layout_type: 0,
            no_data : 0,
            news: [],
            lang: 'auto'
        }
    }

    componentDidMount(){
        
    }
    render(){
        
        return (
            <View style={styles.container}>
              <Text>Here</Text>
            </View>
        );
    }
}

HomeScreen.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      marginTop: Constants.statusBarHeight
    },
});