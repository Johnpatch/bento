import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback, Keyboard, TextInput, AsyncStorage, KeyboardAvoidingView } from 'react-native';
import { showToast } from '../constants/Global';
import { signIn } from '../constants/Api';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import * as SecureStore from 'expo-secure-store';
import { Actions } from 'react-native-router-flux';
export default class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userId: '',
            password: '',
            loginErr: false,
            loaded: true
        };
    }
    componentDidMount(){
        
    }
    
    loginUser(){
        if(this.state.userId != '' && this.state.password != ''){
            this.setState({loaded: false});
            this.setState({loginErr: false})
            signIn(this.state.userId, this.state.password)
            .then(async (response) => {
                console.log(response)
                if(response.data == false){
                    this.setState({loaded: true});
                    showToast('ログインに失敗しました!');
                    this.setState({loginErr: true})
                    return;
                } else{
                    this.setState({loaded: true});
                    this.setState({loginErr: false})
                    //await AsyncStorage.setItem('user', JSON.stringify(response.data[0]));
                    Actions.push('home')
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            });
            
        }else{
            this.setState({loginErr: true})
        }
        
    }
    render(){
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView bebehavior="padding"  style={styles.container}>
                    <TouchableWithoutFeedback  onPress={Keyboard.dismiss}>
                        <View style={styles.bodyContainer}>
                            <View style={{width: '80%', alignItems: 'center'}}>
                                <Text style={styles.loginLabel}>会員ログイン</Text>
                                <View style={styles.sticker}></View>
                            </View>
                            <TextInput 
                                placeholder="ユーザー"
                                placeholderTextColor='rgba(0, 0, 0, 0.4 )'
                                returnKeyType="next"
                                autoCapitalize="none"
                                keyboardType='email-address'
                                autoCorrect={false}
                                style={this.state.loginErr? [styles.input, styles.invalid] : [styles.input]} 
                                onChangeText={userId=>this.setState({userId})}
                                onSubmitEditing={() => this.txtPwd.focus()}
                                />
                            <TextInput 
                                placeholder="パスワード"
                                placeholderTextColor='rgba(0, 0, 0, 0.4 )'
                                secureTextEntry
                                returnKeyType="go"
                                autoCapitalize="none"
                                style={this.state.loginErr? [styles.input, styles.invalid] : [styles.input]} 
                                ref={ref => {this.txtPwd = ref;}}
                                onChangeText={password=>this.setState({password})}
                                onSubmitEditing={() => this.loginUser()}
                            />
                            
                            <View style={{width: '100%'}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 20, paddingHorizontal: 15}}>
                                    <TouchableOpacity onPress={() => Actions.push("phonelogin")}>
                                        <Text>電話番号からログイン</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => Actions.push("forgotpwd")}>
                                        <Text>パスワード忘れた場合</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                    <TouchableOpacity onPress={() => this.loginUser()} style={{borderRadius: 3, width: '80%',backgroundColor:'#645ff0', }}>
                                        <Text style={styles.btnText}>ログイン </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{paddingTop: 50, alignItems: 'center'}}>
                                    <TouchableOpacity onPress={() => Actions.push("signup")}>
                                        <Text>アカウントがございませんか？新規登録してください。</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </View>
        );
    }
    
}

Login.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  bodyContainer: {
      flexGrow: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  input: {
      height: 50,
      backgroundColor: 'rgba(255, 255, 255, 0.4 )',
      marginBottom: 20,
      paddingHorizontal: 10,
      borderColor: '#bcbcbc',
      borderWidth: 1,
      width: "80%",
      borderRadius: 3
  },
  btnText: {
      padding: 15,
      width: "100%",
      color: '#fff',
      textAlign: 'center',
  },
  invalid: {
    borderWidth: 1,
    borderColor: 'red'
  },
  loginLabel: {
      fontSize: 30,
      paddingBottom: 50,
  },
  sticker: {
      width: '100%',
      height: 1,
      backgroundColor: 'gray',
      marginBottom: 50
  }
});
