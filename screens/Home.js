import React from 'react';
import { StyleSheet, View, } from 'react-native';
import BottomTabNavigator from '../navigation/BottomTabNavigator';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LinkingConfiguration from '../navigation/LinkingConfiguration';

const Stack = createStackNavigator();
export default class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {   
        }
    }
    
    render(){
        return (
            <View style={styles.container}>
                <NavigationContainer linking={LinkingConfiguration}>
                    <Stack.Navigator>
                        <Stack.Screen name="Home" component={BottomTabNavigator} />
                    </Stack.Navigator>
                </NavigationContainer>
            </View>
        );
    }
}

Home.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
});