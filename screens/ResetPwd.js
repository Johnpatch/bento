import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback, Keyboard, TextInput, AsyncStorage, KeyboardAvoidingView } from 'react-native';
import { showToast } from '../constants/Global';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import * as SecureStore from 'expo-secure-store';
export default class ResetPwd extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userId: '',
            password: '',
            userErr: false,
            pwdErr: false,
            loaded: true
        };
    }
    componentDidMount(){
        
    }
    
    render(){
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView bebehavior="padding"  style={styles.container}>
                    <TouchableWithoutFeedback  onPress={Keyboard.dismiss}>
                        <View style={styles.bodyContainer}>
                            <View style={{width: '80%', alignItems: 'center'}}>
                                <Text style={styles.loginLabel}>パスワードリセット</Text>
                                <View style={styles.sticker}></View>
                            </View>
                            <TextInput 
                                placeholder="新規パスワード"
                                placeholderTextColor='rgba(0, 0, 0, 0.4 )'
                                returnKeyType="next"
                                autoCapitalize="none"
                                secureTextEntry={true}
                                style={this.state.userErr? [styles.input, styles.invalid] : [styles.input]} 
                                onChangeText={userId=>this.setState({userId})}
                                onSubmitEditing={() => this.txtPwd.focus()}
                                />
                            
                            <TextInput 
                                placeholder="パスワード確認"
                                placeholderTextColor='rgba(0, 0, 0, 0.4 )'
                                returnKeyType="go"
                                secureTextEntry={true}
                                autoCapitalize="none"
                                style={this.state.pwdErr? [styles.input, styles.invalid] : [styles.input]} 
                                ref={ref => {this.txtPwd = ref;}}
                                onChangeText={password=>this.setState({password})}
                                onSubmitEditing={() => this.loginUser()}
                            />
                            
                            <View style={{width: '100%', marginTop: 20}}>
                                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{alignItems: 'flex-start', width: '80%'}}>ログイン画面に戻る</Text>
                                    <TouchableOpacity onPress={() => this.loginUser()} style={{borderRadius: 3, width: '80%',backgroundColor:'#645ff0', }}>
                                        <Text style={styles.btnText}>パスワードリセット</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{paddingTop: 50, alignItems: 'center'}}>
                                    <TouchableOpacity onPress={() => Actions.push("signup")}>
                                        <Text>アカウントがございませんか？新規登録してください。</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </View>
        );
    }
    
}

ResetPwd.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  bodyContainer: {
      flexGrow: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  input: {
      height: 50,
      backgroundColor: 'rgba(255, 255, 255, 0.4 )',
      marginBottom: 20,
      paddingHorizontal: 10,
      borderColor: '#bcbcbc',
      borderWidth: 1,
      width: "80%",
      borderRadius: 3
  },
  btnText: {
      padding: 15,
      width: "100%",
      color: '#fff',
      textAlign: 'center',
  },
  invalid: {
    borderWidth: 1,
    borderColor: 'red'
  },
  loginLabel: {
      fontSize: 30,
      paddingBottom: 50,
  },
  sticker: {
      width: '100%',
      height: 1,
      backgroundColor: 'gray',
      marginBottom: 50
  },
  codeSendBtn: {
    width: '50%',
    height: 50,
    backgroundColor: 'rgba(255, 255, 255, 0.4 )',
    marginBottom: 20,
    borderColor: '#d3d1fa',
    borderWidth: 1,
    borderRadius: 3, 
    alignItems: 'center', 
    justifyContent: 'center'
  }
});
