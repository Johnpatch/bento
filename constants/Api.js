let base_url = 'http://104.156.230.143:2083/api/';
let _headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
function createCall(path, data = null, token = null, headers = {}, method = 'POST') {
    const merged = {
        ..._headers,
        ...headers,
    };

    let body = {};
    if (data) {
        body = {
            ...body,
            ...data,
         };
    }
    if (token) {
        body.api_token = token;
    }
    let strData = JSON.stringify({data: body});
    return fetch(
        `${base_url}${path}`, {
            method,
            headers: merged,
            body: strData,
        },
    ).then((resp) => resp.json());
}

export function signIn(username, password){
    return createCall(
        'company/auth/login/Signin',
        {username, password}
    );
}
