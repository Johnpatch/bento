const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#78849e',
  tabIconSelected: '#3497fd',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
